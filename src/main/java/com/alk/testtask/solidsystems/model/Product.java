package com.alk.testtask.solidsystems.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by Alk on 26.06.2015.
 */
@Entity
@Table(name = "product")
public class Product extends BaseEntity {


    @Column
    private String itemName;

    @Column
    private String description;

    @Column
    private BigDecimal price;

    @Column
    private int count;

    public Product() {
    }

    public Product(String itemName, String description, BigDecimal price, int count) {
        this.itemName = itemName;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "itemName='" + itemName + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
