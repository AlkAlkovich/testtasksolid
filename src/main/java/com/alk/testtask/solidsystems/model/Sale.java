package com.alk.testtask.solidsystems.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Alk on 26.06.2015.
 */
@Entity
@Table(name = "sale")
public class Sale extends BaseEntity {

    @Column
    private Timestamp dateSale;

    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;
    @Column
    private int amount;
    @Column
    private String comment;

    public Sale() {
    }

    public Sale(Timestamp dateSale, Product product, int amount, String comment) {
        if(dateSale==null){
            this.dateSale=new Timestamp(System.currentTimeMillis());
        }else {
            this.dateSale = dateSale;
        }
        this.product = product;
        this.amount = amount;
        this.comment = comment;
    }
    public Sale(Timestamp dateSale, int amount, String comment) {
        if(dateSale==null){
            this.dateSale=new Timestamp(System.currentTimeMillis());
        }else {
            this.dateSale = dateSale;
        }
        this.amount = amount;
        this.comment = comment;
    }

    public Timestamp getDateSale() {
        return dateSale;
    }

    public void setDateSale(Timestamp dateSale) {
        this.dateSale = dateSale;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sale sale = (Sale) o;

        if (amount != sale.amount) return false;
        if (dateSale != null ? !dateSale.equals(sale.dateSale) : sale.dateSale != null) return false;
        if (product != null ? !product.equals(sale.product) : sale.product != null) return false;
        return !(comment != null ? !comment.equals(sale.comment) : sale.comment != null);

    }

    @Override
    public int hashCode() {
        int result = dateSale != null ? dateSale.hashCode() : 0;
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "dateSale=" + dateSale +
                ", product=" + product +
                ", amount=" + amount +
                ", comment='" + comment + '\'' +
                '}';
    }
}
