package com.alk.testtask.solidsystems.controller;

import com.alk.testtask.solidsystems.model.Product;
import com.alk.testtask.solidsystems.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Alk on 02.07.2015.
 */
@Controller
@EnableWebMvc
@RequestMapping(value = "product")
public class ProductController {

    @Autowired
    private ProductService service;

    @RequestMapping("/home")
    public ModelAndView home(ModelMap modelMap){
        modelMap.put("products",service.getAll());
        return new ModelAndView("product",modelMap);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public Product saveProduct(Product product){
        service.save(product);
        return product;
    }
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView deletProduct(@PathVariable String id){
        service.delete(Integer.parseInt(id));
        return new ModelAndView("redirect:product/home");
    }


//    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody List<Product> getAll() {
//
//        return service.getAll();
//    }

}
