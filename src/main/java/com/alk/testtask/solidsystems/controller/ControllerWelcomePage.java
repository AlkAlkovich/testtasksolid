package com.alk.testtask.solidsystems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Alk on 29.06.2015.
 */
@Controller
@RequestMapping("/")
public class ControllerWelcomePage {
    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC - Hello World");
        return "hello";
    }

    @RequestMapping(value = "/helloAjax",method = RequestMethod.GET)
    public @ResponseBody
    String helloAjax(){
        return "Hello Ajax";
    }

    @RequestMapping(value = "/jasonTest",method = {RequestMethod.GET,RequestMethod.POST})
    public @ResponseBody String ajaxJsonRequest(HttpServletRequest request){
        String message=request.getParameter("exampleInputName2");
        System.out.println("test method");
        return "Hello "+message;
    }
}
