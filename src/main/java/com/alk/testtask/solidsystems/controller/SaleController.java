package com.alk.testtask.solidsystems.controller;

import com.alk.testtask.solidsystems.model.Sale;
import com.alk.testtask.solidsystems.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Alk on 03.07.2015.
 */
@RestController
@EnableWebMvc
@RequestMapping("sale")
public class SaleController {

    @Autowired
    private SaleService saleService;


    @RequestMapping("/")
    public ModelAndView home(ModelMap modelMap) {
        modelMap.put("sales", saleService.getAll());
        return new ModelAndView("sale", modelMap);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, headers = "Accept=application/json")
    public
    @ResponseBody
    List<Sale> getAll() {
        return saleService.getAll();
    }

    @RequestMapping(value = "/saveS", method = RequestMethod.POST)
    public
    Sale save(Sale sale) {
        saleService.save(sale);
        return sale;
    }
}
