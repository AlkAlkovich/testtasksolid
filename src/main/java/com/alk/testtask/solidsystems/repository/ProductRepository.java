package com.alk.testtask.solidsystems.repository;

import com.alk.testtask.solidsystems.model.Product;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
public interface ProductRepository<T extends Serializable>{
    void save(Product e);
    List<Product> getAll();
    void delete(int i);
    Product get(int i);

}
