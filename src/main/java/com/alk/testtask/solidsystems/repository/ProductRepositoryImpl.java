package com.alk.testtask.solidsystems.repository;

import com.alk.testtask.solidsystems.model.Product;
import com.alk.testtask.solidsystems.repository.proxy.ProxyProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
@Repository
public class ProductRepositoryImpl implements ProductRepository<Product> {

    @Autowired
    private ProxyProductRepository repository;

    @Override
    public void save(Product e) {
        repository.save(e);
    }

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

    @Override
    public void delete(int i) {
        repository.delete(i);
    }

    @Override
    public Product get(int i) {
        return repository.getOne(i);
    }
}
