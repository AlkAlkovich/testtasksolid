package com.alk.testtask.solidsystems.repository;

import com.alk.testtask.solidsystems.model.Sale;
import com.alk.testtask.solidsystems.repository.proxy.ProxySaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
@Repository
public class SaleRepositoryImpl implements SaleRepository<Sale> {
    @Autowired
    private ProxySaleRepository repository;

    @Override
    public void save(Sale e) {
        repository.save(e);
    }

    @Override
    public List<Sale> getAll() {
        return repository.findAll();
    }
}
