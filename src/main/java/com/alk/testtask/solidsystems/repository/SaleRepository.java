package com.alk.testtask.solidsystems.repository;

import com.alk.testtask.solidsystems.model.Sale;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
public interface SaleRepository <T extends Serializable>{
    void save(Sale e);
    List<Sale> getAll();
}
