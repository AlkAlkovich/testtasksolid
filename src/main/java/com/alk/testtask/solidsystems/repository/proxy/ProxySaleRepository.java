package com.alk.testtask.solidsystems.repository.proxy;

import com.alk.testtask.solidsystems.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
public interface ProxySaleRepository extends JpaRepository<Sale, Integer> {

    @Override
    Sale save(Sale s);

    @Override
    List<Sale> findAll();
}
