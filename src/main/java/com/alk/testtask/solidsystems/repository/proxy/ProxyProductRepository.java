package com.alk.testtask.solidsystems.repository.proxy;

import com.alk.testtask.solidsystems.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */

public interface ProxyProductRepository extends JpaRepository<Product, Integer> {
    @Override
    Product save(Product s);

    @Override
    List<Product> findAll();

    @Override
    void delete(Integer id);

    @Override
    Product getOne(Integer integer);
}
