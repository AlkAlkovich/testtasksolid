package com.alk.testtask.solidsystems.service;

import com.alk.testtask.solidsystems.model.Product;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
public interface ProductService {
    void save(Product e);

    List<Product> getAll();

    void delete(int i);

    Product get(int i);
}
