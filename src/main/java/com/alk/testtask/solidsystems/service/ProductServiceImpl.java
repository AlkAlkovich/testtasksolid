package com.alk.testtask.solidsystems.service;

import com.alk.testtask.solidsystems.model.Product;
import com.alk.testtask.solidsystems.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    public void save(Product e) {
        repository.save(e);
    }

    @Override
    public List<Product> getAll() {
        return repository.getAll();
    }

    @Override
    public void delete(int i) {
        repository.delete(i);
    }

    @Override
    public Product get(int i) {
        return repository.get(i);
    }
}
