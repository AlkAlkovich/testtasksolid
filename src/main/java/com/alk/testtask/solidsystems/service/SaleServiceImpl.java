package com.alk.testtask.solidsystems.service;

import com.alk.testtask.solidsystems.model.Sale;
import com.alk.testtask.solidsystems.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    private SaleRepository repository;

    @Override
    public void save(Sale e) {
        System.out.println(e.toString());
        repository.save(e);
    }

    @Override
    public List<Sale> getAll() {
        return repository.getAll();
    }
}
