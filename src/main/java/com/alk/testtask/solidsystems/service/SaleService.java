package com.alk.testtask.solidsystems.service;

import com.alk.testtask.solidsystems.model.Sale;

import java.util.List;

/**
 * Created by Alk on 29.06.2015.
 */
public interface SaleService {
    void save(Sale e);

    List<Sale> getAll();

}
