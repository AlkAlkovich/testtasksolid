$(document).ready(function () {

    $('#saveProduct').submit(function (e) {

        $.ajax({
            type: 'POST',
            url: '/product/save',
            //contentType: "application/json; charset=utf-8",
            //dataType: 'json',
            data: $(this).serialize(),
            success: function (product) {

                $('#example').last().append(
                    '<tr>' +
                    '<td>' + product.id + '</td>' +
                    '<td>' + product.itemName + '</td>' +
                    '<td>' + product.description + '</td>' +
                    '<td>' + product.count + '</td>' +
                    '</tr>'
                );
            }
        });

        clearInputs();

        e.preventDefault();
    });

});

function clearInputs() {
    $('input[id*="Input"]').each(function () {
        $(this).val('');
    });
}
$(document).ready(function () {
    $('#del').click(function () {
        $('#example tbody').on('click', 'tr', function () {
            var name = $('td', this).eq(0).text();
            $.ajax({
                type: 'GET',
                url: "/product/delete/" + name,
                //dataType:'json',
                success: function (product) {

                    $('#example').last().append(
                        '<tr>' +
                        '<td>' + product.id + '</td>' +
                        '<td>' + product.itemName + '</td>' +
                        '<td>' + product.description + '</td>' +
                        '<td>' + product.count + '</td>' +
                        '</tr>'
                    );
                    //var contact =
                    //    "id : " + result.id +
                    //    " | name : " + result.firstName + " " + result.lastName +
                    //    " | age : " + result.age;
                    //
                    //$("#theJson").html(contact);

                    //clearInputs();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Contact " + textStatus + " " + errorThrown + " !");
                }

            });
        });


    });
});
function updateByData() {
    $('#example').dataTable({
        paging: false,
        searching: false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/product/home",
            "type": "GET",
            "dataType": "json"
        }
        //"columns": [
        //    {"data": ""},
        //    {"data": "itemName"},
        //    {"data": "description"},
        //    {"data": "price"},
        //    {"data": "count"}
        //]
    });
}
