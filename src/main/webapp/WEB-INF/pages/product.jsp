<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Alk
  Date: 02.07.2015
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Spring MVC and Ajax :Product</title>


    <%--<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">--%>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.css">
    <!-- jQuery -->
    <script type="text/javascript" charset="utf8" src="/resources/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" charset="utf8" src="/resources/js/bootstrap.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/plug-ins/1.10.7/api/fnReloadAjax.js"></script>

    <script type="text/javascript" charset="utf8" src="/resources/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>
    <style type="text/css">
        table {
            width: 500px;
        }
    </style>
<head>

</head>
<script src="<c:url value="/resources/js/product.js"/>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').dataTable();

    });
</script>
</head>
<body>
<div id="container">
    <%--<h2>Find Contact By Name</h2>--%>

    <%--<div>--%>
    <%--<label for="firstInput">First Name</label>--%>
    <%--<input type="text" name="firstName" id="firstInput"/>--%>
    <%--</div>--%>
    <%--<div>--%>
    <%--<label for="lastInput">Last Name</label>--%>
    <%--<input type="text" name="lastName" id="lastInput"/>--%>
    <%--</div>--%>
    <%--<div id="theJson"></div>--%>
    <%--<button type="button" id="button">Find</button>--%>

    <h2>Submit new Product</h2>

    <form id="saveProduct">
        <div>
            <label for="itemInput">Item</label>
            <input type="text" name="itemName" id="itemInput"/>
        </div>
        <div>
            <label for="description">Description</label>
            <input type="text" name="description" id="description"/>
        </div>
        <div>
            <label for="priceInput">Price</label>
            <input type="text" name="price" id="priceInput"/>
        </div>
        <div>
            <label for="countInput">Count</label>
            <input type="text" name="count" id="countInput"/>
        </div>

        <div><input id="submit" type="submit" value="Save Contact"></div>
    </form>

    <br><br>

    <h2>Table Contact</h2>
    <%--<table id="contactTableResponse" class="table tr">--%>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="98%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Item</th>
            <th>Desription</th>
            <th>Price</th>
            <th>Count</th>
            <th>Action</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Item</th>
            <th>Desription</th>
            <th>Price</th>
            <th>Count</th>
            <th>Action</th>
            <%--<th scope="row">Spring-Ajax</th>--%>
            <%--<td colspan="5">JQuery Ajax</td>--%>
        </tr>
        </tfoot>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <td>${product.id}</td>
                <td>${product.itemName}</td>
                <td>${product.description}</td>
                <td>${product.price}</td>
                <td>${product.count}</td>
                <td>
                    <button type="button" class="btn btn-primary">Update</button>

                    <button type="button" id="del" class="btn btn-danger">Delete</button>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to save changes you made to document before closing?</p>

                <p class="text-warning">
                    <small>If you don't save, your changes will be lost.</small>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<%--<a href="/spring-ajax/contact/load">JQuery By Load Method</a>--%>
</body>
</html>
