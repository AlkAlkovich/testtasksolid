<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Alk
  Date: 03.07.2015
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css" class="init">

        body {
            font-size: 140%;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/tabletools/2.2.4/css/dataTables.tableTools.css">
    <link rel="stylesheet" type="text/css"
          href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="//editor.datatables.net/examples/resources/bootstrap/editor.bootstrap.css">
    <%--<link rel="stylesheet" type="text/css" href="">--%>
</head>
<body>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>id</th>
        <th>dateSale</th>
        <th>product</th>
        <th>amount</th>
        <th>comment</th>
        <%--<th>id</th>--%>
        <%--<th>iteName</th>--%>
        <%--<th>description</th>--%>
        <%--<th>price</th>--%>
        <%--<th>count</th>--%>
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th>id</th>
        <th>dateSale</th>
        <th>product</th>
        <th>amount</th>
        <th>comment</th>
        <%--<th>id</th>--%>
        <%--<th>iteName</th>--%>
        <%--<th>description</th>--%>
        <%--<th>price</th>--%>
        <%--<th>count</th>--%>
    </tr>
    </tfoot>
</table>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>

<a data-toggle="modal" href="#rating-modal">Write a Review</a>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="detailsForm">
                    <input type="text" hidden="hidden" id="id" name="id">

                    <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                            <label for="product" class="control-label col-xs-3">Product</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="product" name="product"
                                       placeholder="product">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comment" class="control-label col-xs-3">Comment</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="comment" name="comment" placeholder="comment">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="amount" class="control-label col-xs-3">Amount</label>

                        <div class="col-xs-9">
                            <input type="amount" class="form-control" id="amount" name="amount" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<div class="hide fade modal" id="rating-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h2>Your Review</h2>
    </div>

    <div class="modal-body">
        <!-- The async form to send and replace the modals content with its response -->
        <form class="form-horizontal well" data-async data-target="#rating-modal" action="/some-endpoint" method="POST">
            <fieldset>
                <!-- form content -->
            </fieldset>
        </form>
    </div>

    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    </div>
</div>
<!-- Custom form for adding new records -->

</body>
<script type="text/javascript" charset="utf8" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" charset="utf8" src="/resources/js/bootstrap.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        var table = $('#example').dataTable({
            "ajax": {
                url: '/sale/getAll',
                dataSrc: ""
            },
            "columns": [
                {"data": "id"},
                {"data": "dateSale"},
                {"data": "product.itemName"},
                {"data": "amount"},
                {"data": "comment"}
            ]
        });

        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').focus()


        });

        $('#detailsForm').submit(function () {
            $.ajax({
                type: "POST",
                url: '/sale/saveS',
//            contentType: "application/json;charset=utf-8",

                data: $(this).serialize(),
            dataType: 'json',
                success: function (data) {
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });

    });

    function save() {

    }

</script>
</html>
