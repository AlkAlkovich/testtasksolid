DROP TABLE IF EXISTS sale;
DROP TABLE IF EXISTS product;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START 100000;

CREATE TABLE product (
  id            INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  itemName    VARCHAR(45),
  description VARCHAR(45),
  price       NUMERIC NOT NULL ,
  count INTEGER NOT NULL
);
CREATE TABLE sale
(
  id   INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
  dateSale TIMESTAMP,
  product INTEGER NOT NULL ,
  amount INTEGER NOT NULL ,
  comment VARCHAR(100),
  FOREIGN KEY (product)REFERENCES product(id)

);
